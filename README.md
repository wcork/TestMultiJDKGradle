Test Multi-JDK Gradle
=====================
[![build status](https://ci.gitlab.com/projects/5218/status.png?ref=master)](https://ci.gitlab.com/projects/5218?ref=master)

A simple example of building separate jars depending on which jdk built them.

In this example, jdk8 is used as the default build while jdk7 is explicitly defined.

The tests require one of the user parameters *jdk7Home* and *jdk8Home* to be 
defined in *gradle.properties*.

An example of which may be:

*~/.gradle/gradle.properties:*
```groovy
jdk8Home="/Library/Java/JavaVirtualMachines/jdk1.8.0_51.jdk/Contents/Home"
jdk7Home="/Library/Java/JavaVirtualMachines/jdk1.7.0_79.jdk/Contents/Home"
```
If no properties are defined, Gradle will build on the system JDK without a classifier 
on the output jar.


Planned improvements
--------------------
* Continuous integration that works with tests. This would involve a docker image 
that contains all the jdks and the necessary gradle.properties. (May not be necessary).


### **Credits**

This implementation and test code is a more modern implementation based on: https://github.com/rwinch/gradle-multi-jdk.git