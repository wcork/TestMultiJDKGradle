package test;

import org.junit.Test;

import java.io.DataInputStream;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class Java7VersionMessageServiceTest
{
  @Test
  public void getMessage()
  {
    String javaVersion = new JavaVersionMessageService().getMessage();
    assertTrue(javaVersion.startsWith("1.7."));
  }

  @Test
  public void checkBytes() throws Exception
  {
    assertClassVersion("test/JavaVersionMessageService.class");
    assertClassVersion("test/Java7VersionMessageServiceTest.class");
  }

  private void assertClassVersion(String classLocation) throws Exception
  {
    InputStream input = Thread.currentThread().getContextClassLoader().getResourceAsStream(classLocation);
    try
    {
      DataInputStream data = new DataInputStream(input);
      data.readInt();
      data.readShort(); // minor
      int major = data.readShort();
      assertEquals(major, 51);
    } finally
    {
      try
      {
        input.close();
      } catch (Exception ignored)
      {
      }
    }
  }
}
